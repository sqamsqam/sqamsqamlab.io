---
title: "Curriculum Vitae"
date: 2021-02-16T02:11:33+13:00
draft: false
---
# Sam Fenton
Kapiti Coast  
New Zealand  
Email: samuel.ross.fenton@gmail.com  
LinkedIn: https://www.linkedin.com/in/samuelrossfenton

## About Me
I taught myself PHP at the age of 14 and have a passion for programming, automation, SaaS and software that scales. I am easy to get along with and enjoy helping my peers succeed. I work well in a team and enjoy the different perspectives from my peers but can happily work independently using my initiative, taking ownership from inception to release and beyond. I am an effective problem solver, able to communicate solutions to technical and non-technical people. Early in my career, I learned "Never assume, Validate", this has allowed me to make informed decisions and communicate with confidence. I highly value integrity in all aspects of my life enabling a high level of trust.

## Work Experience
### **Full-Stack Developer** at ezyVet - Auckland, New Zealand. Dec 2015 – Jan 2021
ezyVet develops and distributes disruptive technology that innovates, automates, and changes the Veterinary market globally through its cloud-based SaaS veterinary practice management software.

In my 5 years at ezyVet, I was a part of the journey from a start-up of fewer than 20 people to a medium-sized business with offices in Auckland, London and Frisco Texas. There was rarely a dull moment at ezyVet due to the fast-paced environment and constantly shifting priorities where I have proven my ability to calmly and pragmatically work through issues while under pressure.

I have been involved with mentoring and leading small teams and was often approached by my peers across the business for assistance. I spent a lot of time reviewing the team's code, suggesting improvements or prompting discussion to dictate the best way forward. I like to promote good practices across the team, often this included peer programming and encouraging participation regardless of skill or experience.

In my first three years, most of my work was focused on quick resolution to critical issues, fixing bugs, building small features and a few large projects. There were some extra responsibilities mixed in such as assisting the support team and looking after the AWS environment.

In the last two years, ezyVet readopted SCRUM and built out a product team and a DevOps team, shifting my responsibilities to be more focused on development. Despite this change, I was quite involved with the product team assisting with planning and scoping projects before they were brought to refinement. I spent a lot less time managing the AWS environment but still got my hands dirty from time to time helping the DevOps team with various tasks such as adding to or changing the CI/CD pipeline and jumping in to help with environment or performance issues.

Outside of my development duties I also managed an internal process that facilitated the needs of other departments to lighten the overall supporting role of the development team by acting as a sort of gatekeeper to the Jira backlog. My focus was always on replication and next steps to keep things moving forward if more discovery was required before an issue was ready to be pushed through to Jira.

#### Key Projects
* **SMS Gateway**  
I reengineered the SMS gateway to make use of multiple providers (Twilio, SMS Global and Modica Group) with the primary goals of reducing costs and ensuring the system was reliable with the ability to quickly fall back to alternatives in the event one or more providers experienced downtime. The SMS gateway facilitated two way messaging directly into ezyVet.

* **Student Verification**  
This project consisted of gathering requirements from the client (Cornell University College of Veterinary Medicine), to understand their workflows and how we could best integrate them into ezyVet in a way that makes sense for all clients. My personal highlight of this project was the opportunity to be a part of the two-week onsite implementation team at Cornell University.

* **Steakholder Email Update Service**  
I lead a team of 5 developers and introduced them to Golang. Our goal was to develop a serverless solution which could scale utilizing AWS Lambda, Amazon SQS, Amazon SES and a CloudWatch Timer to kick everything off on a daily schedule. This started the adoption of Golang into ezyVet's stack and the team was able to pick up the new language and ship within a single two-week sprint.

* **Client Migration Between Regions**  
This was one of the more interesting projects we picked up for the DevOps team which involved automating the process of moving a client from one region or database server to another using a GitLab CI pipeline.

#### Partners
Over the years I built and maintained many integrations with 3rd party's serving a variety of purposes while also communicating directly with partners maintaining excellent rapport. Some of the technology used for integration include REST, SOAP and various other custom HTTP API's, often lacking in documentation.

* **Product Supplier Integrations**  
Allow clients to send purchase orders and receive invoices electronically, Nightly automated price updates direct from the supplier and tracking of stock to generate purchase orders with minimal user interaction.

* **Diagnostic Integrations**  
Electronic ordering of diagnostic tests and retrieval of results. I have worked with both in-house lab systems and external reference labs. Some of these integrations required building and maintaining small applications inside the client's network to facilitate communication with the cloud environment.

* **Medical Imaging Integrations (PACS, MWL, RIS)**  
Enable clients to send study requests to their modality worklist, receiving study results directly into ezyVet and requesting interpretations of existing or future studies.

### Other Experience
Before starting at ezyVet I did some contract work migrating an old PHP4 e-commerce site to OpenCart for Mannings Sports, an Australian Sport and Hunting Equipment Distributor. This involved writing migration scripts in SQL to bring their existing data into OpenCart and template changes to fit the company branding.

## Technical Skills
* Proficient in PHP, Golang and C#
* MySQL and MSSQL database administration and optimization
* Unit testing and mocking with PHPUnit, MSTest and Golang's built-in testing package
* Experience producing custom reports from a wide range of data to drive decisions
* Experience with cloud environments and auto-scaling (AWS, DigitalOcean)
* Serverless using AWS Lambda and Amazon SQS
* Enough JavaScript to get by for most front-end tasks (jQuery, React)
* Proficient in managing and deploying Linux servers through SSH
* Experience managing virtualised environments using Docker, KVM and LXC
* Source Control experience using Git with light exposure to SVN and Team Foundation Server

## Education
* **Bachelor of Information and Communications Technology (Applied) (Level 7)**, UCOL Palmerston North, Palmerston North, New Zealand. July 2011 – November 2015

* **Certificate for Advanced Computer Users (Level 4)**, UCOL Palmerston North, Palmerston North, New Zealand. February 2011 - July 2011

## Interests and Hobbies
Outdoor activities I enjoy include sailing, fishing, kayaking and long-boarding.

To relax I like to spend time with my friends and family or participating in team orientated games.

Lately, my focus has been on software design, Kubernetes, Serverless and improving my understanding of Golang.

## References
*Available upon request*